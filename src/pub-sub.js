/*globals require, define, module */

(function (globals) {
    'use strict';

    var check = require('check-types'),

    archetypalEvent = createEvent({ name: 'archetype' }),

    eventBrokers = {},

    functions = {
        createEvent: createEvent,
        getEventBroker: getEventBroker
    };

    exportFunctions();

    function createEvent (args) {
        var name, data, callback, self;

        check.verifyObject(args, 'Invalid arguments');

        name = args.name;
        data = args.data || {};
        callback = args.callback;

        check.verifyUnemptyString(name, 'Invalid name');
        if (typeof callback !== 'undefined') {
            check.verifyFunction(callback, 'Invalid callback');
        }

        self = {
            getName: getName,
            getData: getData,
            respond: respond
        };

        return self;

        function getName () {
            return name;
        }

        function getData () {
            return data;
        }

        function respond () {
            if (callback) {
                callback.apply(self, arguments);
            }
        }
    }

    function getEventBroker (id) {
        var subscriptions = {
            '*': []
        };

        check.verifyUnemptyString(id, 'Invalid id');

        if (typeof eventBrokers[id] === 'undefined') {
            eventBrokers[id] = {
                subscribe: subscribe,
                unsubscribe: unsubscribe,
                publish: publish
            };
        }

        return eventBrokers[id];

        function subscribe (args) {
            verifyArgs(args);
            addSubscription(args.name, args.callback);
        }

        function verifyArgs (args) {
            check.verifyObject(args, 'Invalid arguments');
            check.verifyUnemptyString(args.name, 'Invalid name');
            check.verifyFunction(args.callback, 'Invalid callback');
        }

        function addSubscription (eventName, callback) {
            if (typeof subscriptions[eventName] === 'undefined') {
                subscriptions[eventName] = [];
            }

            subscriptions[eventName].push(callback);
        }

        function unsubscribe (args) {
            verifyArgs(args);
            removeSubscription(args.name, args.callback);
        }

        function removeSubscription (eventName, callback) {
            var i, eventSubscriptions = subscriptions[eventName];

            if (check.isArray(eventSubscriptions) === false) {
                return;
            }

            for (i = 0; i < eventSubscriptions.length; i += 1) {
                if (eventSubscriptions[i] === callback) {
                    eventSubscriptions.splice(i, 1);
                    break;
                }
            }
        }

        function publish (event) {
            check.verifyQuack(event, archetypalEvent, 'Invalid event');

            notifySubscriptions(event, '*');
            notifySubscriptions(event, event.getName());
        }

        function notifySubscriptions (event, eventName) {
            var eventSubscriptions = subscriptions[eventName], i;

            if (check.isArray(eventSubscriptions)) {
                for (i = 0; i < eventSubscriptions.length; i += 1) {
                    eventSubscriptions[i](event);
                }
            }
        }
    }

    function exportFunctions () {
        if (typeof define === 'function' && define.amd) {
            define(function () {
                return functions;
            });
        } else if (typeof module !== 'undefined' && module !== null) {
            module.exports = functions;
        } else {
            globals.pubsub = functions;
        }
    }
}(this));

