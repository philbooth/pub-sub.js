'use strict';

var exec = require('child_process').exec,

commands = {
    minify: './node_modules/.bin/uglifyjs ./src/pub-sub.js --compress --mangle --output ./src/pub-sub.min.js',
    test: './node_modules/.bin/mocha --ui tdd --reporter spec --colors --slow 50 ./test',
    lint: './node_modules/.bin/jshint ./src/pub-sub.js --config config/jshint.json',
    prepare: 'npm install'
};

desc('Minify the source code for deployment.');
task('minify', function () {
    runTask('minify', 'Minifying...');
}, {
    async: true
});

desc('Run the unit tests against the source code.');
task('test', function () {
    runTask('test', 'Testing the source code...');
}, {
    async: true
});

desc('Lint the source code.');
task('lint', function () {
    runTask('lint', 'Linting the source code...');
}, {
    async: true
});

desc('Install dependencies.');
task('prepare', function () {
    runTask('prepare', 'Preparing the build environment...');
}, {
    async: true
});

function runTask (command, message) {
    console.log(message);
    runCommand(command);
}

function runCommand (command) {
    exec(commands[command], { cwd: __dirname }, function (error, stdout, stderr) {
        console.log(stdout);
        if (typeof error === 'object' && error !== null) {
            console.log(error.message);
            process.exit(1);
        }
        complete();
    });
}

