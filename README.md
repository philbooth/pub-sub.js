# pub-sub.js

A tiny library that implements pub/sub in JavaScript.

[![Build status][ci-image]][ci-status]

## Installation

### Via NPM

```
npm install pub-sub
```

### Via Jam

```
jam install pub-sub
```

### Via Git

```
git clone git@github.com:philbooth/pub-sub.js.git
```

## Usage

### Loading the library

Both
CommonJS
(e.g.
if you're running on [Node.js][node]
or in the browser with [Browserify])
and AMD
(e.g. if you're using [Require.js][require])
loading styles are supported.
If neither system is detected,
the library defaults to
exporting it's interface globally
as `pubsub`.

### Calling the exported functions

The library exports two functions,
`createEvent` and `getEventBroker`.
Event objects returned by `createEvent` expose three methods:
`getName`, `getData` and `respond`.
Event broker objects returned by `getEventBroker` expose three methods:
`subscribe`, `unsubscribe` and `publish`.

### createEvent (args)

Returns an event object
that can be published via the event broker.
`args` must be an object
with a required `name` property,
`name` being a string used to identify the event.
The `args` object may also contain two optional properties:
`data`, which can be of any type,
and `callback` which must be a function if specified.

#### event.getName ()

Returns the event name
that was specified in the call to `createEvent`.

#### event.getData ()

Returns the data property
that may optionally have been specified in the call to `createEvent`.

#### event.respond (...)

Calls the `callback` function
that may optionally have been specified in the call to `createEvent`.

### getEventBroker (id)

Returns an event broker object
that can be used to subscribe to and publish events.
The `id` argument is a string
that identifies the event broker,
enabling callers to easily access the same event broker
from different parts of the code if they so wish.

#### eventBroker.subscribe (args)

Subscribe to an event.
`args` is an object with two required properties:
`name`, a string identifying the event to subscribe to;
and `callback`, a function that will be called when that event occurs.
If the name propery is `'*'`,
the supplied callback function will be subscribed to every event that occurs,
regardless of event name.

#### eventBroker.unsubscribe (args)

Unsubscribe a previously subscribed callback.
The `args` object is the same format as was passed to `subscribe`.

#### eventBroker.publish (event)

Publish an event to subscribers.
`event` is an object returned by the `createEvent` function.

## Development

### Dependencies

The build environment relies on
Node.js,
[NPM],
[Jake],
[Mocha],
[Chai]
and [Spooks.js][spooks].
Assuming that you already have Node.js and NPM set up,
you just need to run `npm install`
to install all of the dependencies
as listed in `package.json`.

### Unit tests

The unit tests are in `test/pub-sub.js`.
You can run them with the command `npm test` or `jake test`.

[ci-image]: https://secure.travis-ci.org/philbooth/pub-sub.js.png?branch=master
[ci-status]: http://travis-ci.org/#!/philbooth/pub-sub.js
[node]: http://nodejs.org/
[browserify]: https://github.com/substack/node-browserify
[require]: http://requirejs.org/
[npm]: https://npmjs.org/
[jake]: https://github.com/mde/jake
[mocha]: http://visionmedia.github.com/mocha
[chai]: http://chaijs.com/
[spooks]: https://github.com/philbooth/spooks.js

