(function () {
    'use strict';

    var assert = require('chai').assert,
        spooks = require('spooks');

    suite('no setup:', function () {
        test('require does not throw', function () {
            assert.doesNotThrow(function () {
                require('../src/pub-sub');
            });
        });

        test('require returns object', function () {
            assert.isObject(require('../src/pub-sub'));
        });
    });

    suite('require:', function () {
        var pubsub;

        setup(function () {
            pubsub = require('../src/pub-sub');
        });

        teardown(function () {
            pubsub = undefined;
        });

        test('createEvent function is defined', function () {
            assert.isFunction(pubsub.createEvent);
        });

        test('calling createEvent without arguments throws', function () {
            assert.throws(function () {
                pubsub.createEvent();
            });
        });

        test('calling createEvent with name argument does not throw', function () {
            assert.doesNotThrow(function () {
                pubsub.createEvent({ name: 'foo' });
            });
        });

        test('calling createEvent with empty name argument throws', function () {
            assert.throws(function () {
                pubsub.createEvent({ name: '' });
            });
        });

        test('calling createEvent with null throws', function () {
            assert.throws(function () {
                pubsub.createEvent(null);
            });
        });

        test('calling createEvent with unnamed object throws', function () {
            assert.throws(function () {
                pubsub.createEvent({});
            });
        });

        test('calling createEvent with all arguments set does not throw', function () {
            assert.doesNotThrow(function () {
                pubsub.createEvent({ name: 'bar', data: {}, callback: function () {} });
            });
        });

        test('calling createEvent with all arguments set except name throws', function () {
            assert.throws(function () {
                pubsub.createEvent({ data: {}, callback: function () {} });
            });
        });

        test('calling createEvent with non-function callback throws', function () {
            assert.throws(function () {
                pubsub.createEvent({ name: 'foo', data: {}, callback: {} });
            });
        });

        test('createEvent returns an object', function () {
            assert.isObject(pubsub.createEvent({ name: 'bar' }));
        });

        test('createEvent with all arguments set returns an object', function () {
            assert.isObject(pubsub.createEvent({ name: 'foo', data: {}, callback: function () {} }));
        });

        test('createEvent with the same name returns a different object', function () {
            assert.notStrictEqual(pubsub.createEvent({ name: 'foo' }), pubsub.createEvent({ name: 'foo' }));
        });

        suite('create event foo:', function () {
            var event;

            setup(function () {
                event = pubsub.createEvent({ name: 'foo' });
            });

            teardown(function () {
                event = undefined;
            });

            test('event has method called getName', function () {
                assert.isFunction(event.getName);
            });

            test('calling getName method without arguments does not throw', function () {
                assert.doesNotThrow(function () {
                    event.getName();
                });
            });

            test('calling getName method returns foo', function () {
                assert.strictEqual(event.getName(), 'foo');
            });

            test('event has method called getData', function () {
                assert.isFunction(event.getData);
            });

            test('calling getData method without arguments does not throw', function () {
                assert.doesNotThrow(function () {
                    event.getData();
                });
            });

            test('calling getData method returns object', function () {
                assert.isObject(event.getData());
            });

            test('calling getData method returns object with no properties', function () {
                var property, data = event.getData(), count = 0;
                for (property in data) {
                    if (data.hasOwnProperty(property)) {
                        count += 1;
                    }
                }
                assert.strictEqual(count, 0);
            });

            test('event has method respond', function () {
                assert.isFunction(event.respond);
            });

            test('calling respond method does not throw', function () {
                assert.doesNotThrow(function () {
                    event.respond();
                });
            });
        });

        suite('create event bar:', function () {
            var event;

            setup(function () {
                event = pubsub.createEvent({ name: 'bar' });
            });

            teardown(function () {
                event = undefined;
            });

            test('calling getName method returns bar', function () {
                assert.strictEqual(event.getName(), 'bar');
            });
        });

        suite('create event baz with non-default arguments:', function () {
            var log, event;

            setup(function () {
                log = {}
                event = pubsub.createEvent({
                    name: 'baz',
                    data: {
                        foo: 'bar',
                        baz: 'qux'
                    },
                    callback: spooks.fn({
                        name: 'callback',
                        log: log
                    })
                });
            });

            teardown(function () {
                log = event = undefined;
            });

            test('calling getName method returns baz', function () {
                assert.strictEqual(event.getName(), 'baz');
            });

            test('calling getData method returns object with two properies', function () {
                var property, data = event.getData(), count = 0;
                for (property in data) {
                    if (data.hasOwnProperty(property)) {
                        count += 1;
                    }
                }
                assert.strictEqual(count, 2);
            });

            test('calling getData method returns value bar for property foo', function () {
                assert.strictEqual(event.getData().foo, 'bar');
            });

            test('calling getData method returns value qux for property baz', function () {
                assert.strictEqual(event.getData().baz, 'qux');
            });

            test('callback function was not called', function () {
                assert.strictEqual(log.counts.callback, 0);
            });

            suite('call respond method with argument foo:', function () {
                setup(function () {
                    event.respond('foo');
                });

                test('callback function was called once', function () {
                    assert.strictEqual(log.counts.callback, 1);
                });

                test('callback function was passed one argument', function () {
                    assert.lengthOf(log.args.callback[0], 1);
                });

                test('callback function was passed argument foo', function () {
                    assert.strictEqual(log.args.callback[0][0], 'foo');
                });

                test('callback function was applied to event', function () {
                    assert.strictEqual(log.these.callback[0], event);
                });

                suite('call respond method with argument bar:', function () {
                    setup(function () {
                        event.respond('bar');
                    });

                    test('callback function was called twice', function () {
                        assert.strictEqual(log.counts.callback, 2);
                    });

                    test('callback function was passed one argument', function () {
                        assert.lengthOf(log.args.callback[1], 1);
                    });

                    test('callback function was passed argument bar', function () {
                        assert.strictEqual(log.args.callback[1][0], 'bar');
                    });

                    test('callback function was applied to event', function () {
                        assert.strictEqual(log.these.callback[1], event);
                    });
                });
            });
        });

        test('getEventBroker function is defined', function () {
            assert.isFunction(pubsub.getEventBroker);
        });

        test('calling getEventBroker without arguments throws', function () {
            assert.throws(function () {
                pubsub.getEventBroker();
            });
        });

        test('calling getEventBroker with id argument does not throw', function () {
            assert.doesNotThrow(function () {
                pubsub.getEventBroker('foo');
            });
        });

        test('calling getEventBroker with empty id argument throws', function () {
            assert.throws(function () {
                pubsub.getEventBroker('');
            });
        });

        test('calling getEventBroker with non-string id argument throws', function () {
            assert.throws(function () {
                pubsub.getEventBroker({ id: 'foo' });
            });
        });

        test('getEventBroker returns an object', function () {
            assert.isObject(pubsub.getEventBroker('bar'));
        });

        test('getEventBroker with a different id returns a different object', function () {
            assert.notStrictEqual(pubsub.getEventBroker('foo'), pubsub.getEventBroker('bar'));
        });

        test('getEventBroker with the same id returns the same object', function () {
            assert.strictEqual(pubsub.getEventBroker('foo'), pubsub.getEventBroker('foo'));
        });

        suite('get event broker foo:', function () {
            var eventBroker;

            setup(function () {
                eventBroker = pubsub.getEventBroker('foo');
            });

            teardown(function () {
                eventBroker = undefined;
            });

            test('event broker has method called subscribe', function () {
                assert.isFunction(eventBroker.subscribe);
            });

            test('calling subscribe method without arguments throws', function () {
                assert.throws(function () {
                    eventBroker.subscribe();
                });
            });

            test('calling subscribe method with name foo and callback function does not throw', function () {
                assert.doesNotThrow(function () {
                    eventBroker.subscribe({ name: 'foo', callback: function () {} });
                });
            });

            test('calling subscribe method with empty name and callback function throws', function () {
                assert.throws(function () {
                    eventBroker.subscribe({ name: '', callback: function () {} });
                });
            });

            test('calling subscribe method with name foo and non-function callback throws', function () {
                assert.throws(function () {
                    eventBroker.subscribe({ name: 'foo', callback: {} });
                });
            });

            test('event broker has method called unsubscribe', function () {
                assert.isFunction(eventBroker.unsubscribe);
            });

            test('calling unsubscribe method without arguments throws', function () {
                assert.throws(function () {
                    eventBroker.unsubscribe();
                });
            });

            test('calling unsubscribe method with name bar and callback function does not throw', function () {
                assert.doesNotThrow(function () {
                    eventBroker.unsubscribe({ name: 'bar', callback: function () {} });
                });
            });

            test('calling unsubscribe method with empty name and callback function throws', function () {
                assert.throws(function () {
                    eventBroker.unsubscribe({ name: '', callback: function () {} });
                });
            });

            test('calling unsubscribe method with name bar and non-function callback throws', function () {
                assert.throws(function () {
                    eventBroker.unsubscribe({ name: 'bar', callback: {} });
                });
            });

            test('event broker has method called publish', function () {
                assert.isFunction(eventBroker.publish);
            });

            test('calling publish method without arguments throws', function () {
                assert.throws(function () {
                    eventBroker.publish();
                });
            });

            test('calling publish method with event does not throw', function () {
                assert.doesNotThrow(function () {
                    eventBroker.publish(pubsub.createEvent({ name: 'foo' }));
                });
            });

            test('calling publish method with empty object throws', function () {
                assert.throws(function () {
                    eventBroker.publish({});
                });
            });

            suite('call subscribe method with argument foo:', function () {
                var log, callback;

                setup(function () {
                    log = {};
                    callback = spooks.fn({
                        name: 'callback',
                        log: log
                    });
                    eventBroker.subscribe({
                        name: 'foo',
                        callback: callback
                    });
                });

                teardown(function() {
                    log = callback = undefined;
                });

                test('callback function was not called', function () {
                    assert.strictEqual(log.counts.callback, 0);
                });

                suite('call publish method with foo event:', function () {
                    var event;

                    setup(function () {
                        event = pubsub.createEvent({ name: 'foo' });
                        eventBroker.publish(event);
                    });

                    teardown(function () {
                        event = undefined;
                    });

                    test('callback function was called once', function () {
                        assert.strictEqual(log.counts.callback, 1);
                    });

                    test('callback function was passed one argument', function () {
                        assert.lengthOf(log.args.callback[0], 1);
                    });

                    test('callback function was passed event argument', function () {
                        assert.strictEqual(log.args.callback[0][0], event);
                    });
                });

                suite('call publish method with bar event:', function () {
                    setup(function () {
                        eventBroker.publish(pubsub.createEvent({ name: 'bar' }));
                    });

                    test('callback function was not called', function () {
                        assert.strictEqual(log.counts.callback, 0);
                    });
                });

                suite('call subscribe method with argument foo then publish method with foo event:', function () {
                    var event;

                    setup(function () {
                        eventBroker.subscribe({
                            name: 'foo',
                            callback: spooks.fn({
                                name: 'secondCallback',
                                log: log
                            })
                        });
                        event = pubsub.createEvent({ name: 'foo' });
                        eventBroker.publish(event);
                    });

                    teardown(function () {
                        event = undefined;
                    });

                    test('first callback function was called once', function () {
                        assert.strictEqual(log.counts.callback, 1);
                    });

                    test('first callback function was passed one argument', function () {
                        assert.lengthOf(log.args.callback[0], 1);
                    });

                    test('first callback function was passed event', function () {
                        assert.strictEqual(log.args.callback[0][0], event);
                    });

                    test('second callback function was called once', function () {
                        assert.strictEqual(log.counts.secondCallback, 1);
                    });

                    test('second callback function was passed one argument', function () {
                        assert.lengthOf(log.args.secondCallback[0], 1);
                    });

                    test('second callback function was passed event', function () {
                        assert.strictEqual(log.args.secondCallback[0][0], event);
                    });
                });

                suite('call unsubscribe with argument foo and same callback then publish foo:', function () {
                    setup(function () {
                        eventBroker.unsubscribe({ name: 'foo', callback: callback });
                        eventBroker.publish(pubsub.createEvent({ name: 'foo' }));
                    });

                    test('callback function was not called', function () {
                        assert.strictEqual(log.counts.callback, 0);
                    });
                });

                suite('call unsubscribe with argument foo and different callback then publish foo:', function () {
                    setup(function () {
                        eventBroker.unsubscribe({ name: 'foo', callback: function () {} });
                        eventBroker.publish(pubsub.createEvent({ name: 'foo' }));
                    });

                    test('callback function was called once', function () {
                        assert.strictEqual(log.counts.callback, 1);
                    });
                });

                suite('call unsubscribe with argument bar and same callback then publish foo:', function () {
                    setup(function () {
                        eventBroker.unsubscribe({ name: 'bar', callback: callback });
                        eventBroker.publish(pubsub.createEvent({ name: 'foo' }));
                    });

                    test('callback function was called once', function () {
                        assert.strictEqual(log.counts.callback, 1);
                    });
                });
            });

            suite('call subscribe method with argument bar then publish method twice with bar event:', function () {
                var log, event;

                setup(function () {
                    log = {};
                    eventBroker.subscribe({
                        name: 'bar',
                        callback: spooks.fn({
                            name: 'callback',
                            log: log
                        })
                    });
                    event = pubsub.createEvent({ name: 'bar' });
                    eventBroker.publish(event);
                    eventBroker.publish(event);
                });

                teardown(function () {
                    log = event = undefined;
                });

                test('callback function was called twice', function () {
                    assert.strictEqual(log.counts.callback, 2);
                });

                test('callback function was passed one argument first time', function () {
                    assert.lengthOf(log.args.callback[0], 1);
                });

                test('callback function was passed event first time', function () {
                    assert.strictEqual(log.args.callback[0][0], event);
                });

                test('callback function was passed one argument second time', function () {
                    assert.lengthOf(log.args.callback[1], 1);
                });

                test('callback function was passed event second time', function () {
                    assert.strictEqual(log.args.callback[1][0], event);
                });
            });

            suite('call subscribe method with argument * then publish method with foo event:', function () {
                var log, event;

                setup(function () {
                    log = {};
                    eventBroker.subscribe({
                        name: '*',
                        callback: spooks.fn({
                            name: 'callback',
                            log: log
                        })
                    });
                    event = pubsub.createEvent({ name: 'foo' });
                    eventBroker.publish(event);
                });

                teardown(function () {
                    log = event = undefined;
                });

                test('callback function was called once', function () {
                    assert.strictEqual(log.counts.callback, 1);
                });

                test('callback function was passed one argument', function () {
                    assert.lengthOf(log.args.callback[0], 1);
                });

                test('callback function was passed event argument', function () {
                    assert.strictEqual(log.args.callback[0][0], event);
                });

                suite('call publish method with bar event:', function () {
                    var barEvent;

                    setup(function () {
                        barEvent = pubsub.createEvent({ name: 'bar' });
                        eventBroker.publish(barEvent);
                    });

                    teardown(function () {
                        barEvent = undefined;
                    });

                    test('callback function was called twice', function () {
                        assert.strictEqual(log.counts.callback, 2);
                    });

                    test('callback function was passed one argument first time', function () {
                        assert.lengthOf(log.args.callback[0], 1);
                    });

                    test('callback function was passed foo event argument first time', function () {
                        assert.strictEqual(log.args.callback[0][0], event);
                    });

                    test('callback function was passed one argument second time', function () {
                        assert.lengthOf(log.args.callback[1], 1);
                    });

                    test('callback function was passed foo event argument second time', function () {
                        assert.strictEqual(log.args.callback[1][0], barEvent);
                    });
                });
            });
        });
    });
}());

